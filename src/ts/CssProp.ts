/**
 * A CSS key-value pair. Currently both are arbitrary and don't prevent
 * nonsense CSS rules.
 *
 * Example: `new CssProp('color', 'red');`
 */

export class CssProp {
  constructor(
    readonly name: string,
    readonly value: string
  ) {}
}
