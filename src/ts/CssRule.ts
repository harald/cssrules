import { CssProp } from './CssProp.js';

/**
 * Combines a CSS selector with a list of CSS properties. Use the static methods
 * to create
 * - {@link host rules for a host element}, to
 * - {@link detail subselect a host element} (like with :hover) or for a
 * - {@link child child element}.
 */

export class CssRule {
  /**
   * CSS properties of this rule.
   */
  readonly props: ReadonlyArray<CssProp>;

  /**
   * The selector of the rule. It will start with a space character if it is
   * supposed to select child (or descendent) elements.
   */
  readonly selector: string;

  /**
   * Creates a rule with an empty selector, meaning it should apply to the
   * host element.
   */
  static host(...props: CssProp[]): CssRule {
    return new CssRule('', ...props);
  }

  /**
   * Creates a rule with the given detail selector. The selector will be
   * written to a style element such that it subselects the root element. For
   * example if combined with a css class `Boo` a selector like `:hover` will
   * be combined into `.Boo:hover`.
   */
  static detail(selector: string, ...props: CssProp[]): CssRule {
    selector = selector.replace(/\s*/, '');
    return new CssRule(selector, ...props);
  }

  /**
   * Creates a rule with a child (or descendent selector). In particular when
   * combined with a css class `Boo` a selector like `div` will be combined
   * into `.Boo div`, i.e. with a space before the given selector.
   *
   * To address a direct child, use `>` like in `CssRules.child('>div', ...)`.
   */
  static child(selector: string, ...props: CssProp[]): CssRule {
    return new CssRule(' ' + selector, ...props);
  }

  /**
   * @param selector blublub
   */
  private constructor(selector: string, ...props: CssProp[]) {
    this.props = props;
    this.selector = selector;
  }
}
