/**
 * Your main entry point is {@link CssRules.createElement}, to create an
 * HTML element with the necessary CSS class names and to register the
 * style rules on the web page.
 * @module
 */

import { CssProp } from './CssProp';
import { CssRule } from './CssRule';

/**
 * Describes an object that provides us with data we need to register css rules
 * on the page.
 */
export interface CssRulesProvider {
  /** The css class name to apply. This string should not have a leading dot. */
  CSSCLASS: string;
  /** The css rules to register in `<head>`. */
  CSSRULES: CssRule[];
}

/**
 * Creates `<style>` elements in `<head>` with css rules and provides
 * {@link createElement} much like `document.createElement` but adds the
 * necessary CSS class right away.
 */
export class CssRules {
  private readonly known = new Set<string>();
  private styleElement: HTMLStyleElement | undefined;

  /**
   * Creates the css rules maintainer.
   *
   * @param styleStrategy is the strategy to use when adding
   * rules to the `<head><style>`
   * element. With `api`, `CSSStyleSheet.insertRule()` is used with the
   * downside, that the style element will not show the rule text. With
   * `text`, the rules are added as text to the generated style element and can
   * be easily inspected with the browser's developer toolbox.
   */
  constructor(private readonly styleStrategy: 'api' | 'text' = 'text') {}

  /**
   * Creates an html element with `document.createElement`, adds the css class
   * from the rules provider and installs the rules if this is the first time we
   * see the css class.
   */
  createElement<K extends keyof HTMLElementTagNameMap>(
    tagName: K,
    rulesProvider: CssRulesProvider
  ): HTMLElementTagNameMap[K] {
    this.initCssRules(rulesProvider);
    const element = document.createElement(tagName);
    element.classList.add(rulesProvider.CSSCLASS);
    return element;
  }

  /**
   * Registers the given css rules at the front of the page's `<head>`
   * element if this was not done for the {@link CssRulesProvider.CSSCLASS}
   * already.
   *
   * If {@link createElement} is called at least once, calling this method is
   * not necessary.
   */
  initCssRules(rulesProvider: CssRulesProvider): void {
    if (this.known.has(rulesProvider.CSSCLASS)) {
      return;
    }
    this.known.add(rulesProvider.CSSCLASS);
    for (const rule of rulesProvider.CSSRULES) {
      this.add('.' + rulesProvider.CSSCLASS + rule.selector, rule.props);
    }
  }

  private add(selector: string, rules: ReadonlyArray<CssProp>): void {
    let rule = `${selector} {\n`;
    for (const p of rules) {
      rule = rule + '  ' + p.name + ': ' + p.value + ';\n';
    }
    rule += '}\n';
    const styleEl = this.assureStyleElement();
    if (this.styleStrategy === 'api') {
      this.addApiStyle(styleEl, rule);
    } else {
      this.addTextStyle(styleEl, rule);
    }
  }

  private addTextStyle(styleEl: HTMLStyleElement, rule: string): void {
    styleEl.textContent += rule;
  }

  private addApiStyle(styleEl: HTMLStyleElement, rule: string): void {
    // https://stackoverflow.com/a/24662178
    const stylesheet = styleEl.sheet as CSSStyleSheet;
    stylesheet.insertRule(rule, 0);
  }

  private assureStyleElement(): HTMLStyleElement {
    if (this.styleElement !== undefined) {
      return this.styleElement;
    }
    const el = document.createElement('style');
    document.head.prepend(el);
    // https://stackoverflow.com/a/24662178
    return el;
  }
}
